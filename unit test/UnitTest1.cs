﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using konsol_app_cnet;
using System.Runtime.InteropServices;

namespace unit_test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestGetInt()
        {
            Assert.AreEqual(52,Program.getInt());
        }
        [TestMethod]
        public void TestGetString()
        {
            Assert.AreEqual("Hello", Marshal.PtrToStringAnsi(Program.getString()));
        }
    }
}
